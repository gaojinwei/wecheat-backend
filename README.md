### 添加朋友实现逻辑

1. 依赖[socket.io](https://socket.io/)
2. 登录后，客户端新建一个 socket，并 emit 一个 register 事件，同时加上用户标识的参数，就用用户 id 即可。
3. 服务端对连接的 socket 监听 register 事件，用一个变量记录所有连接的 socket，key 为用户标识，value 为该 socket 连接。
4. 客户端 socket emit addFriendRequest 事件即向指定用户发送好友请求，必须追加请求来自哪里和去往何处的参数(消息来自哪个用户 id，发往哪个用户 id)。
5. 服务端监听 addFriendRequest 事件，并获得消息信息(最重要的是消息来源和去处)，在第 3 步已经存储了所有 socket，于是可由服务端向消息去处的 socket 继续 emit addFriendRequest 事件。
6. 客户端监听 addFriendRequest 事件，以服务端为中介收到了消息，接下来取得数据刷新 ui 即可。
7. 以上为好友请求发送方通知接收方，接下来为接收方将同意消息通知到原请求方
8. 客户端同意添加好友请求后，emit newFriendNotify 事件并指明需要通知方
9. 服务端监听 newFriendNotify 事件，向需要通知方 emit newFriendNotify 事件
10. 客户端监听 newFriendNotify 事件，收到对方已同意添加好友请求的消息。

### 发布

- server: AWS es2，注意，es2 默认只开放sh登录方式，如要进行后续的http访问以及开放相应端口需要编辑安全组。
- 必须先在服务器装好node环境，node安装一如既往推荐nvm安装方式，然后全局安装pm2，因为需要使用pm2管理node进程。
- 借助gitlab的ci工具，每次commit到master分支自动更新代码并重启服务，ci流程参看.gitlab-ci.yml。
- 发布脚本参照deploy目录
- 需要在gitlab项目中设置ci的目的地server ip以及sh访问时的private key
- 如果没记错，在服务器中需要通过sh去gitlab download代码，于是，不光gitlab需要保存访问服务器时需要的private key，服务器也需要保存访问gitlab的private key。