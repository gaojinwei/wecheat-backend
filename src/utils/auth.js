import config from "../config";
import User from "../resources/user/user.model";
import Room from "../resources/room/room.model";
import Message from "../resources/message/message.model";
import jwt from "jsonwebtoken";

export const newToken = user => {
  return jwt.sign({ id: user.id }, config.secrets.jwt, {
    expiresIn: config.secrets.jwtExp
  });
};

export const verifyToken = token =>
  new Promise((resolve, reject) => {
    jwt.verify(token, config.secrets.jwt, (err, payload) => {
      if (err) return reject(err);
      resolve(payload);
    });
  });

export const signup = async (req, res) => {
  if (!req.body.userName || !req.body.password) {
    return res.status(400).send({ error: "需要用户名或密码" });
  }
  try {
    var user;
    if (req.body.userName !== "WeCheat") {
      const wecheat = await User.findOne({ userName: "WeCheat" });
      user = await User.create({ ...req.body, friends: [wecheat.id] });
      await User.update(wecheat, { $push: { friends: user._id } });
      const defaultMessage = await Message.create({
        user: wecheat.id,
        message_body: "你好，有任何问题欢迎反馈，反正我也不改～"
      });

      const defaultRoom = await Room.create({
        users: [user.id, wecheat.id],
        messages: [defaultMessage.id]
      });
    } else {
      user = await User.create(req.body);
    }

    const token = newToken(user);
    return res.status(201).send({ token, id: user.id });
  } catch (e) {
    console.error(e);
    return res.status(400).end();
  }
};

export const signin = async (req, res) => {
  if (!req.body.userName || !req.body.password) {
    return res.status(400).send({ error: "需要用户名或密码" });
  }

  const user = await User.findOne({ userName: req.body.userName });
  if (!user) {
    return res.status(401).end();
  }

  try {
    const match = await user.checkPassword(req.body.password);
    if (!match) {
      return res.status(401).send({ message: "密码错误" });
    }

    const token = newToken(user);
    res.status(201).send({ token, id: user.id });
  } catch (e) {
    console.error(e);
    return res.status(401).send({ message: "密码错误" });
  }
};

export const protect = async (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).send({ message: "no auth" });
  }

  let token = req.headers.authorization.split("Bearer")[1];

  if (!token) {
    return res.status(401).send({ message: "no auth" });
  }

  try {
    const payload = await verifyToken(token);
    const user = await User.findById(payload.id)
      //返回的对象不带password字段
      .select("-password -friends")
      //转为js对象
      .lean()
      .exec();
    req.user = user;
    //鉴权成功后允许访问api
    next();
  } catch (e) {
    console.error(e);
    return res.status(401).send({ message: "no auth" });
  }
};
