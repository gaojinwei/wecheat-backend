import express from "express";
import { json, urlencoded } from "body-parser";
import morgan from "morgan";
import config from "./config";
import cors from "cors";
import cloudinary from "cloudinary";
import formData from "express-form-data";
import http from "http";
import socketIo from "socket.io";
import dotenv from "dotenv";
dotenv.config();

import { connect } from "./utils/db";
import User from "./resources/user/user.model";
import userRouter from "./resources/user/user.router";
import roomRouter from "./resources/room/room.router";
import messageRouter from "./resources/message/message.router";
import { signup, signin, protect } from "./utils/auth";

export const app = express();
const server = http.createServer(app);
const io = socketIo(server);
// io.on("connection", () => {
//   console.log("receive");
// });
var connectedUsers = {};

io.on("connection", socket => {
  //对于客户端的socket连接按照room进行分组，每当客户端触发chat事件，
  //服务端通知到该room下所有socket
  const roomId = socket.handshake.query.roomId;
  if (roomId) {
    socket.join(roomId);
    socket.on("chat", function(data) {
      io.to(roomId).emit("chat");
    });
  } else {
    socket.on("register", function(userId) {
      socket.userId = userId;
      connectedUsers[userId] = socket;
    });
    socket.on("addFriendRequest", function(clientData) {
      const { from, to, message, userDetailInfo } = clientData;
      if (connectedUsers.hasOwnProperty(to)) {
        connectedUsers[to].emit("addFriendRequest", {
          userDetailInfo,
          message,
          from
        });
      }
    });
    socket.on("newFriendNotify", function(to) {
      if (connectedUsers.hasOwnProperty(to)) {
        connectedUsers[to].emit("newFriendNotify");
      }
    });
  }
});

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});

app.disable("x-powered-by");

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(morgan("dev"));

app.post("/signup", signup);
app.post("/signin", signin);
//app.use(formData.parse())
app.post("/image-upload", [protect, formData.parse()], (req, res) => {
  const user = req.user;
  const values = Object.values(req.files);

  const promises = values.map(image => cloudinary.uploader.upload(image.path));

  Promise.all(promises)
    .then(async results => {
      const imgUrl = results[0].secure_url;
      const imgId = results[0].public_id;
      const userUpdated = await User.findByIdAndUpdate(
        user._id,
        { avatar: imgUrl, avatarId: imgId },
        { new: true }
      )
        .select("-password")
        .exec();

      //默认头像不可删除
      if (user.avatarId !== "pqwadasunjaww22z7lkk") {
        cloudinary.v2.uploader.destroy(user.avatarId);
      }
      res.status(200).json(userUpdated);
    })
    .catch(err => res.status(400).json(err));
});

app.use("/api", protect);

app.use("/api/user", userRouter);
app.use("/api/room", roomRouter);
app.use("/api/message", messageRouter);

app.get("/", () => {
  console.log("request coming");
});

export const start = async () => {
  try {
    await connect();
    server.listen(config.port, () => {
      console.log(`REST API on http://localhost:${config.port}/api`);
    });
  } catch (e) {
    console.error(e);
  }
};
