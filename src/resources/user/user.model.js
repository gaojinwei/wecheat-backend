const mongoose = require("mongoose");
import bcrypt from "bcrypt";

//可以追加参数：{timestamps: true}，mongoose会添加创建时间和更新时间
//不要定义id字段，mongo中本身要用
const userSchema = new mongoose.Schema(
  {
    //   uid: {
    //     type: Number,
    //     required: true
    //     //unique: true
    //   },
    userName: {
      type: String,
      required: true,
      trim: true,
      maxlength: 10
      //unique: true
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 6,
      maxlength: 20
    },
    avatar: {
      type: String,
      default:
        "https://res.cloudinary.com/dhbi5p4aa/image/upload/v1562478759/pqwadasunjaww22z7lkk.jpg"
    },
    avatarId: {
      type: String,
      default: "pqwadasunjaww22z7lkk"
    },
    friends: {
      type: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "user"
        }
      ]
      //default: [mongoose.Types.ObjectId("5d13a003c17a9346b1139489")]
    }
  },
  { timestamps: true }
);

// userSchema.index(
//   {
//     uid
//   },
//   { unique: true }
// );

userSchema.index(
  {
    userName: 1
  },
  { unique: true }
);

userSchema.pre("save", function(next) {
  if (!this.isModified("password")) {
    return next();
  }

  bcrypt.hash(this.password, 8, (err, hash) => {
    if (err) {
      return next(err);
    }

    this.password = hash;
    next();
  });
});

userSchema.methods.checkPassword = function(password) {
  const passwordHash = this.password;
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, passwordHash, (err, same) => {
      if (err) {
        return reject(err);
      }

      resolve(same);
    });
  });
};

const User = mongoose.model("user", userSchema);

//module.exports = User;
export default User;
