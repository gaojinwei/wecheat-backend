import { Router } from "express";
import userControllers, {
  getUserFriends,
  getUserByName,
  assentToFriendRequest
} from "./user.controllers";

const { getOne, updateOne } = userControllers;
const router = Router();

router.get("/", getOne);
router.put("/", updateOne);

router.get("/:userId/friends", getUserFriends);
router.get("/single", getUserByName);

router.post("/makeNewFriend", assentToFriendRequest);

export default router;
