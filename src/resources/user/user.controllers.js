import User from "./user.model";
import Room from "../room/room.model";
import Message from "../message/message.model";
import { crudControllers } from "../../utils/crud";
export default crudControllers(User);

export const getUserFriends = async (req, res) => {
  const userId = req.params.userId;

  const user = await User.findOne({ _id: userId });

  if (!user) {
    return res.status(404).end();
  }

  //如果用select()，也只能接受一个参数
  const friends = await User.find(
    { _id: { $in: user.friends } },
    //"userName password"
    //只返回userName和_id，加avatar
    "userName avatar"
  );

  //不用写成{data: friends}的形式，前端用axios时会自动包装一层
  res.json({ friends, user: req.user });
};

export const getUserByName = async (req, res) => {
  const userName = req.query.userName;

  const user = await User.findOne({ userName }, "userName avatar _id");

  if (!user) {
    return res.status(404).end();
  }

  res.json({ user });
};

export const assentToFriendRequest = async (req, res) => {
  const receiver = req.user._id;
  const sender = req.body.whoseRequest;
  const receiverDoc = await User.findOne({ _id: receiver });
  const senderDoc = await User.findOne({ _id: sender });

  if (!receiverDoc || !senderDoc) {
    return res.status(404).end();
  }

  await User.update(receiverDoc, { $push: { friends: sender } });
  await User.update(senderDoc, { $push: { friends: receiver } });
  const defaultMessage = await Message.create({
    user: receiver,
    message_body: "我通过了你的朋友验证请求，现在我们可以开始说骚话了"
  });

  const defaultRoom = await Room.create({
    users: [receiver, sender],
    messages: [defaultMessage.id]
  });

  res.json({ success: true });
};
