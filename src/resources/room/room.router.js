import { Router } from "express";
import roomControllers, { getRooms, getRoomById } from "./room.controllers";

const { getOne, updateOne } = roomControllers;
const router = Router();

router.get("/", getOne);
router.put("/", updateOne);

router.get("/:userId", getRooms);
router.get("/chatDetail/:roomId", getRoomById);

export default router;
