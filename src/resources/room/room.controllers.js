import Room from "./room.model";
import { crudControllers } from "../../utils/crud";

export default crudControllers(Room);

export const creatRoom = async (req, res) => {
  try {
    // body: {
    //     users: [],
    //     messages: []
    // }
    const room = await Room.create({ ...req.body, created_at: Date.now });
  } catch (e) {
    console.error(e);
    return res.status(400).end();
  }
};

export const getRooms = async (req, res) => {
  //实际在auth时总会在req里加上user信息，不用额外传userid
  const userId = req.params.userId;

  //https://stackoverflow.com/questions/18148166/find-document-with-array-that-contains-a-specific-value
  const rooms = await Room.find({ users: userId })
    .populate(
      "users messages",
      "-password" //populate并且不带上passowrd field
    )
    .sort("-updated_at");

  if (!rooms) {
    return res.status(404).end();
  }

  res.json({ rooms });
};

export const getRoomById = async (req, res) => {
  const roomId = req.params.roomId;
  const userId = req.user._id;

  const currentRoom = await Room.findOne({ _id: roomId }).populate(
    "users messages",
    "-password"
  );

  if (!currentRoom) {
    return res.status(404).end();
  }

  res.json(currentRoom);
};
