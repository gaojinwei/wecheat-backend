import Message from "./message.model";
import Room from "../room/room.model";
import { crudControllers } from "../../utils/crud";

export default crudControllers(Message);

export const creatMessage = async (req, res) => {
  try {
    const message = await Message.create(req.body);
    const roomId = req.params.roomId;

    await Room.findByIdAndUpdate(
      roomId,
      {
        $push: { messages: message._id },
        updated_at: new Date().toISOString()
      },
      { new: true }
    ).exec();
    res.status(200).json({ success: true });
  } catch (e) {
    console.error(e);
    return res.status(400).end();
  }
};
