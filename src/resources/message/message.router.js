import { Router } from "express";
import messageControllers, { creatMessage } from "./message.controllers";

const { getOne, updateOne } = messageControllers;
const router = Router();

router.get("/", getOne);
router.put("/", updateOne);

router.post("/:roomId/newMessage", creatMessage);

export default router;
