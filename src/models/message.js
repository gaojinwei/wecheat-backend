const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  //所属于哪个聊天室
  //room: { type: mongoose.Schema.Types.ObjectId, ref: "room" },
  //何人发送
  user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  message_body: String,
  message_status: { type: Boolean, default: false },
  created_at: { type: Date, default: Date.now }
});

const Message = mongoose.model("message", messageSchema);

module.exports = Message;
