//代表每个聊天列表，由于知道含有哪些用户参与，对于每个用户的聊天室列表，
//只需查询哪些聊天列表含有该用户，并按照updated_at降序，即可得到
//https://stackfame.com/mongodb-chat-schema-mongoose-chat-schema-chat-application

const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
  //name: { type: String, lowercase: true, unique: true },
  topic: String,
  //该聊天室有哪些人参与
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: "user" }],
  messages: [{ type: mongoose.Schema.Types.ObjectId, ref: "message" }],
  created_at: Date,
  updated_at: { type: Date, default: Date.now }
});

const Room = mongoose.model("room", roomSchema);

module.exports = Room;
