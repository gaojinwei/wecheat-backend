const mongoose = require("mongoose");

//可以追加参数：{timestamps: true}，mongoose会添加创建时间和更新时间
//不要定义id字段，mongo中本身要用
const userSchema = new mongoose.Schema({
  uid: {
    type: Number,
    required: true
    //unique: true
  },
  userName: {
    type: String,
    required: true
    //unique: true
  },
  password: {
    type: String,
    required: true
  },
  //引用其他user
  friends: [{ type: mongoose.Schema.Types.ObjectId, ref: "user" }]
});

userSchema.index(
  {
    uid
  },
  { unique: true }
);

userSchema.index(
  {
    userName
  },
  { unique: true }
);

const User = mongoose.model("user", userSchema);

module.exports = User;
