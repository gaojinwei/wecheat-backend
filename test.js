const mongoose = require("mongoose");

const connect = () => {
  return mongoose.connect("mongodb://localhost:27017/backendTest0");
};

const User = require("./src/models/user");

connect()
  .then(async connection => {
    const user = await User.create({
      uid: 2,
      userName: "某",
      password: "gaojinwei"
    });
    const wecheat = await User.create({
      uid: 1,
      userName: "wecheat",
      password: "gaojinwei",
      friends: [user._id]
    });
    //如果查到就返回，否则新建document(upsert)
    // const wecheat = await User.findOneAndUpdate(
    //   {
    //     userName: "wecheat"
    //   },
    //   {
    //     userName: "wecheat"
    //   },
    //   { upsert: true, new: true }
    // );

    const match = await User.findById(wecheat.id)
      .populate("friends")
      .exec();
    console.log(match);

    //const find = await User.find({ userName: "gao" });
    //const findById = await User.findById("dfd");
    //const updated = await User.findByIdAndUpdate("df", {});
  })
  .catch(e => console.error(e));
